import java.util.Scanner;
public class ExamRefactoring {

	public static void main(String[] args) {
		String playAgain = "";
		do {
			userInput();
	        System.out.print("Would you like to run again? Enter Y to run or any other key to quit: ");
	        playAgain = Input.input.next();
	    }
	    while(playAgain.equalsIgnoreCase("Y"));
		System.out.println("Goodbye!");
	}
	
	@SuppressWarnings("unused")
	private static void userInput() {
		System.out.print("Insert diameter of circle: ");
		Circle circlediam;
		circlediam = new Circle();
		System.out.printf("Insert the size of square: ");
		Square squaresize;
		squaresize = new Square();
				
		System.out.printf("Size of circle is: %.2f \n", Circle.AreaCircle);
		System.out.printf("Size of square is: %.2f \n", Square.AreaSquare);
		System.out.println(Comparison.Calc);
	}

	public static class Input{
		static Scanner input = new Scanner(System.in);
	}
	
	public static class Circle{
		static double diameter = Input.input.nextDouble();
		private static double AreaCircle = Calculation(diameter);
		private static double Calculation(double diameter){
			double sum = Math.pow(diameter, 2)*Math.PI;
			return sum;
		}
	}
	
	public static class Square{
		static double size = Input.input.nextDouble();
		private static double AreaSquare = Calculation(size);
		private static double Calculation(double size){
			double sum = Math.pow(size, 2);
			return sum;
		}
	}
	
	public static class Comparison{
		static String Calc = Calculation(Circle.AreaCircle, Square.AreaSquare);
		private static String Calculation(double diameter, double size){
			if (diameter > size) {
				return "Circle is bigger";
			} else {
				return "Square is bigger";
			}
		}
	}
}


